---
layout: dynamic_page
title: ಸುದ್ದಿ
permalink: /ka/blog/
language: ka
---

<div class="row row-margin">
  {% for post in site.posts %}
  {% if post.language == 'ka' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
