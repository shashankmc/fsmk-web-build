---
layout: post
title: "Why we have to learn computer? Session by Gnani Sankaran"
date: 2015-03-13 20:25:16
category: events
---

Gnani is a popular writer in the Tamil language. He is known for frank and uncompromising views on culture and politics, which he has expressed in the media for 30 years. He plays multiple roles in newspapers, magazines, plays, and films. He was the editor of a Tamil magazine Dheemtharikida, O Pakkangal, Balloon, Media Uravugal, Thavippu. He directs more than 30 plays from his theater troupe called "Pareeksha". http://www.gnani.net/ https://en.wikipedia.org/wiki/Gnani_Sankaran Thursday, February 20, 2014at 5:00pm @FSMK Office,Bangalore, India
