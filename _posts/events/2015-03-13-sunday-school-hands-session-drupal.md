---
layout: post
title: "Sunday School  Hands on Session on Drupal"
date: 2015-03-13 20:10:36
category: events
---

Drupal is a free and open-source content-management framework written in PHP and distributed under the GNU General Public License. It is used as a back-end framework for at least 2.1% of all Web sites worldwide.  

Requirements:  
Bring a GNU/Linux Laptop (recommended)  
Have LAMP package installed.  
Instructions given here:  
<a href="https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu" rel="nofollow nofollow" target="_blank">https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu</a>  
or  
<a href="https://help.ubuntu.com/community/ApacheMySQLPHP" rel="nofollow nofollow" target="_blank">https://help.ubuntu.com/community/ApacheMySQLPHP</a>  

Download Drupal:  
<a href="http://ftp.drupal.org/files/projects/drupal-7.31.zip" rel="nofollow nofollow" target="_blank">http://ftp.drupal.org/files/projects/drupal-7.31.zip</a>  
or  
<a href="http://ftp.drupal.org/files/projects/drupal-7.31.tar.gz" rel="nofollow nofollow" target="_blank">http://ftp.drupal.org/files/projects/drupal-7.31.tar.gz</a>  

Speaker: Shijil T V

Sunday, October 12, 2014at 11:00am

@FSMK Office,No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden, Bengaluru - 560030. Ph: 080-42817353

 
