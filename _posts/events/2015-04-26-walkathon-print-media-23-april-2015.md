---
layout: post
title: "Walkathon in the print media 23 April 2015"
date: 2015-04-26 10:42:09
category: events
---

The announcement for the walkathon  
http://www.newindianexpress.com/cities/bengaluru/A-Walk-for-Free-Internet/2015/04/23/article2777720.ece

The reports covering the walkathon that was organised in support of net neutrality

http://timesofindia.indiatimes.com/tech/tech-news/Protests-in-Chennai-Bengaluru-for-net-neutrality/articleshow/47037770.cms  
http://www.aninews.in/newsdetail2/story213124/protests-in-chennai-bengaluru-for-net-neutrality.html  
http://bharatpress.com/2015/04/24/protests-in-chennai-bengaluru-for-net-neutrality/

The mashup video that FSMK made to promote the 23 April campaign  
https://www.youtube.com/watch?v=rr8Dnm_9lsE

The footage from walkathon

https://www.youtube.com/watch?v=orQ9gYRidm0  
https://www.youtube.com/watch?v=Mc6cCcjzy0U  
https://www.youtube.com/watch?v=t0J0tpKZq94  
https://www.youtube.com/watch?v=D2OeMKyVrRE
