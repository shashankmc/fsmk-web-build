---
layout: post
title: "FSMK Contribution Drive"
date: 2015-03-13 19:58:17
category: events
---

<div>
  <div>
    <div>
      <div>
        ##Contribution Drive<br /> <br /> #WHAT<br /> <br /> FSMK Contribution Drive is meant to be a drive that focuses on spreading awareness about free software and free software adoption much like the summer camps. In addition, this drive will primarily focus on contribution to free software projects by volunteers from several fields and backgrounds.<br /> Contribution Drive is an initiative that aims at getting all members associated with FSMK together and help contribute to internal FSMK projects along with other free software projects. If FSMK can manage to spend a few days every year in contributing to the Free world, it would be a powerful statement and a great get-together for all its members and free software adopters.<br /> <br /> #WHY<br /> <br /> The format for a follow-up camp or an advanced camp has always been a tough one to crack despite various discussions. The obvious flaw in the advanced camp is that it is virtually impossible to determine what qualifies as advanced and who and what the size of the target audience is. Summer camps mostly focus on teaching the attendees technologies and programming. The free software world, however, is not all about code, nor are the people. Hence, to expand our reach and connect with people who are simply not meant for coding, we need an alternative format.<br /> <br /> Here is a possible format for conducting camps as part of the brand FSMK camps.<br /> <br /> #WHEN<br /> <br /> The dates decided are 24th - 26th of January at the FSMK Office. It will not be residential. Arrangements should make the arrangements for accomdation coming from outside of Bangalore.<br /> <br /> #WHO<br /> <br /> The obvious target audience here are summer camp participants and all GLUGs under FSMK. The drive, of course, will be open to anybody willing to contribute. But we must ensure all GLUG members and summer camp participants are a part of this.<br /> After the newly formed working professionals and academicians sub-committee, this would be a great opputunity to interact with the rest of the community and start contributing hands-on.<br /> <br /> #HOW<br /> <br /> Every FSMK sub-committee will organise tasks that will engage attendees and guide them in contributing to their chosen projects/activities. Each sub-committee is alloted a slot whose period of time will be decided based on what the sub-committee proposes. The sub-committees can take the opportunity to give a brief introduction about what they do, their achievements and how anybody can contribute. A schedule will be prepared based on this and will be released before hand.
      </div>
    </div>
  </div>
</div>

<div>
   
</div>

<div>
  <div>
    <div>
      <div>
        <div>
          <p>
            January 24 - January 26<br /> Jan 24 at 9:00am to Jan 26 at 6:00pm
          </p>

          <p>
            FSMK Office<br /> Bangalore, India
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
