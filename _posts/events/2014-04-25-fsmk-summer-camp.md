---
layout: post
title: "FSMK Summer Camp "
date: 2014-04-25 18:45:50
category: events
---

It has been decided to organize the summer camp.  The camp will cover basics of FOSS technologies, improvised on previous camp syllabus. soon we will be launching website for the camp.

 

[<img class="alignnone  wp-image-7" src="http://fsmkblog.files.wordpress.com/2014/04/10269336_10152776146912995_904756544085730709_o.jpg?w=300" alt="10269336_10152776146912995_904756544085730709_o" width="323" height="237" />][1]

 [1]: http://fsmkblog.files.wordpress.com/2014/04/10269336_10152776146912995_904756544085730709_o.jpg

 

Camp detail

**Duration: 9 days**  
**Dates: 19 - 27 July, 2014**  
**Target participation: 200 students**

Organizing Teams:

**[Venue / Sponsorship Hunting] = { $Vignesh / Sarath / Jaykumar / Karthik B / Shashank };**  
**[Campaign] = { $Shijil / Rakesh / Hari Prasad / Vijay / Sreejith };**  
**[Web Team] = { $Raghuram / Padma / Hari / Rakesh};**  
**[Content / Syllabus] = { $Vikram / Vijay / Saravanan / Padma / Abrar / Shashank / Sarath / Karthik B };**  
**[Creative] = { $Rahul / Isham };**  
**[Media Liasion] = { $Sarath / Jaykumar / Karthik B };**

Snaps from Previous year summer camp

<https://www.flickr.com/photos/ananda_yana/sets/72157634850717897/>  
<https://www.facebook.com/groups/196435887186229/>

 

 
