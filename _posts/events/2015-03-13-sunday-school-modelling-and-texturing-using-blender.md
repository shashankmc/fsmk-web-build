---
layout: post
title: "Sunday School: Modelling and Texturing using Blender"
date: 2015-03-13 20:28:08
category: events
---

We have earlier covered Basics of Blender in one of the previous Sunday School session last year. This time let us take it from there and learn how to do basic modelling and texturing from <a href="https://www.facebook.com/kaushik.ram3" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Kaushik Ram</a>.   
Please install Blender on your laptop before coming the session. Also do bring an external mouse which makes it easier to use Blender

Sunday, February 9, 2014at 2:00pm - 4:00pm  
@FSMK Office Bangalore, India
