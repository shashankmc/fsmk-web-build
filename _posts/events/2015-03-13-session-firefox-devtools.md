---
layout: post
title: "Session on Firefox DevTools"
date: 2015-03-13 20:39:26
category: events
---

This Sunday School, join us to learn Firefox Devtools by <a href="https://www.facebook.com/kaustav.dasmodak" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Kaustav Das Modak</a>, a <a href="https://www.facebook.com/MozillaReps" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">ReMo - Mozilla Reps</a>. Please download the latest nightly Firefox Package from <a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fnightly.mozilla.org%2F&h=kAQFD8yL1&enc=AZMzO_e5F9BOwBhkWEoywSPSSZZoCmlR6Xy3Gfw1ZxX8fuabwe6psJYd6z4YClquTrs&s=1" rel="nofollow nofollow" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;" target="_blank">http://nightly.mozilla.org/</a> and install the same on your system.

Sunday, December 1, 2013at 2:00pm - 4:30pm  
@FSMK Office, Wilson Garden
