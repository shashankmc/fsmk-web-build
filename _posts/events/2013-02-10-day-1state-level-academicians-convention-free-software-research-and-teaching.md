---
layout: post
title: "Day 1:State Level Academicians Convention on Free Software in Research and Teaching"
date: 2013-02-10 15:50:18
category: events
---

<div dir="ltr" style="text-align: left;">
  <p>
    Karnataka's first State Level Convention for Academicians on Free Software in Research and Teaching has taken off in Bangalore on the 11th March, 2011, organized by the Free Software Movement-Karnataka (FSMK) and Reva Institute of Technology and Management.<br /> The State Convention which will go on for another day, on the 12th of March is aiming at bringing awareness about the use of Free Software utilities in teaching and learning.
  </p>

  <p>
    In this regard, Prof.K Gopinath, from the Indian Institute of Science correlated the manner Science has grown by hypothesis, falsification and verification, and how Freee Software takes this iteration one step futher, and allows sharing of the such refined work in the digital domain.
  </p>

  <p>
    Jayakumar HS, Joint Secretary of Free Software Movement-India, projected this Convention to be the first step in creating a mass movement amongst the teaching and researching faculties. Dr. S Sunil Manvi, Dean R&D of Reva ITM emphasized on the roles such conventions would be playing in enabling the teachers to pursue further research and facilitate students in their academics. The lukewarm response in numbers for the Convention was compensated by the interest all the attendees exhibited while the sessions were in progress. Mr.Vikram Vincent started off the proceedings with an exhilarating talk, giving the insight into the importance of Free Software in Education, by teasing 'the broken education  system', with stats and figures which supported his points. The demonstration of the MOODLE implementation of Christ University, did get the audience excited. In the same session, Mr.Prabodh introduced other ubiquitously used Free Software utilities for the Engineering curriculum, like Codeblocks, Scilab and others.
  </p>

  <p>
    The second talk had Prof.Gopinath talk about Pursuing Research in correlation to Free Software, and how Free Software is the most obvious choice for all researchers to endorse. After the audience grabbed a quick lunch, we did get back on track for the sessions, but with some confusion. Time crunch did manifest itself in an awkward manner. In the interest of the audience, we decided to make the parallel sessions, sequential, and now I realize it should have been otherwise. In anycase, the first two sessions post lunch went on in haste, but not without impacting the audience.<br /> Bhuvan Krishna from Swecha did an encore of his session at IISc's Open day: Spinning one's own GNU/Linux distribution using Debian Live Helper. This session again got the audience interested, and hope some of them take up some responsible tasks on the lines of localization.
  </p>

  <p>
    Electric:The VLSI CAD design tool, was demonstrated by Mr.Ravikumar and Mr.Kamal, from NXG Semiconductors. While this tool is a great utility for Electronics engineers, we did sense the obvious mismatch with the non Electronics audience. Nevertheless, this session requires an elaborate revist.
  </p>

  <p>
    The final session by our own Balaji Kutty, turned out to be undoubtedly the best session of the day with some effective demonstration and very alive interaction with the audience, in his session on GCC/GDB based Programming and Debugging. A no-coding person like myself, was thoroughly enjoying all the tricks he was playing with coding and debugging. Overall, few flaws ( will certainly work upon); Nevertheless, the vibe of Swatantra energy has been surfacing up amongst the participants. We will amplify the same in tomorrow's sessions! The talks through the day, spanning from the role of Free Software in Teaching and Learning, Making custom GNU/Linux distibutions to specific utilities pertaining to Electronics and Computer Science were well received amongst the cream of the faculty members who had turned up for the Convention. With more exciting talks scheduled for day two with an important Panel discussion about incorporating Free Software into the curriculum, day two is also going to be of heavy dosage.
  </p>

  <p>
     
  </p>
</div>
