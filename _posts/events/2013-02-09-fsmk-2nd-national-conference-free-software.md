---
layout: post
title: "FSMK at the 2nd National Conference on Free Software"
date: 2013-02-09 18:55:13
category: events
---

<p align="JUSTIFY">
            Cochin University of Science & Technology ( CUSAT ) was the venue of the second National Conference on Free Software in India on November 15th & 16th , 2008. The first National Conference was held at Hyderabad in 2007. Kerala has been in the forefront of promoting the Free Software through the joint efforts by the Government, various Free Software groups and different mass organizations. This was vindicated by the different events, seminars, policy declarations by the government etc during the event.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  Kerala Education Minister M.A Baby inaugurated the event. In his inaugural address, the minister mentioned that through a government decision, it was made mandatory to use Free Software in Kerala’s school curriculum. Former Supreme court judge V.R Krishna Iyer delivered his message using teleconferencing and told that Free Software has ended the monopoly of all IT MNCs. In the evening, CPI(M) Polit Bureau member and Rajya Sabha MP Sitaram Yechury delivered the inaugural address at a plenary session on “Global financial crisis and its impact on the IT sector”. He elucidated the issue of the crisis in global capitalism and its impact on people and the role of the Free Software Movement in building the resistance. West Bengal IT minister Debasish Ghosh & Kerala Finance Minister Thomas Isaac also spoke. Other eminent personalities from the academician level & politics were present and spoke about the need of Free Software. The next and the last day of the conference, Kerala Chief Minister V.S. Achuthanandan delivered address calling for a joint resistance against capitalist forces trying to monopolise knowledge. Kerala Industries minister Elamaram Kareem told that the State could be turned into a cost-effective destination for the industry by using the advantages of the free software platform. Various important declarations were also made during the National Conference.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  The Conference had variety of seminars on Free Software Philosophy, Social Awareness topics, Science & Technology etc going parallel in different halls. There were also technical sessions on different Free Software tools and technology. Exhibitions on Anti-war, fascism, poverty, ethnic conflict, CIA's role in destabilizing developing worlds, etc were the most inspiring and unforgettable components of the event. Exhibitions participated by about 80-100 stalls would surely highlight the true efforts of organizers in reaching the free software to thousands and thousands of people. It speaks that free software activities are not just limited to some Board-room meetings or class rooms.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  The Conference was well organised and the 1000+ delegate participation from all over the country was magnificent. It was a representation of the growing peoples' mass movement which cannot be stopped or controlled by vested interests or power centres. What is obvious is the conscious conversion of a developers' movement to a peoples' movement that the people are taking charge. The volunteers of the conference which involved largely the students of CUSAT need to be appreciated for doing a great job in organizing a National level event very efficiently.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  <strong>Active involvement by the FSMK led delegation from Karnataka</strong>
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  A group of 50 delegates represented Karnataka in the National Conference. Free Software Movement Karnataka (FSMK) led the group consisting of different groups of IT professionals, researchers, scientists, engineering students, computer operators far away from Gulbarga, academicians, kannada bloggers & children from Ambedkar Community Computer Centre. The diverse nature of the group clearly showed the power of Free Software Movement in bringing together diverse in to fight against the knowledge lock in by monopoly capitalists. FSMK members have spent considerable efforts in mobilizing different groups for the Conference and hence helping to spread the ideas of Free Software to wider sections.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  FSMK members also were involved in taking various sessions and also participated in many of these seminars. Venky took the seminar on “Knowledge Communities and caste”. He explained the pathetic conditions of dalits and the backward classes in Tamil Nadu and also how Free Software technology can be used to enhance the backward communities. Rasinesh handled the session on “Free Software in Media”. He talked on how Free Software Content management systems like Django, Drupal, Joomla, Media Wiki are being used in the media industry to manage content for their websites and presented case studies like news paper website like washingpost.com etc.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  There were Free Software stalls exhibiting variety of Free Softwares. Special mention is required for the participation of the children of AC3 (Ambedkar Community Computer Centre) who was part of FSMK delegation to the National Conference and became the spotlight in the Free Software stalls. AC3 is a slum computer centre in Bangalore to providing computer skills to the kids of the slum with active involvement by FSMK members and other social workers. The power of Free Software was already proved by the introduction of computer classes through Free Software in AC3. Mani, a 9th standard boy of the centre a talented artist explored Free Software to his taste of painting. He mastered GIMP and drew astonishing pictures which was placed in the stalls for both the days of the National Conference. The quality of the pictures made different Free Software activists to folk around the stall to buy his pictures. The stall had coverage by the media who also interviewed Mani on his paintings. Mani told them how Free Software was able to make the difference in the centre and raising the confidence of the kids.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  The involvement in the National conference raised the confidence and knowledge of the FSMK team. The team was able to do good amount of interactions with eminent personalities during the conference and discussed many things. The conference had made a great impact on Mani, Sarasu and Santhosh of AC3 by raising their morale and leaving the conference with much more to do. The entire FSMK led team imbibed the values of Free Software and vowed to spread it among much wider sections of the society.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  <strong>Declarations made during the Conference</strong>
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  Some important declarations were made during the National Conference:
</p>

<p align="JUSTIFY">
  <em>The Department of Information Technology has also decided to set up an international centre for free software in the State.The declaration adopted at the two-day national meet urged the governments to procure free software in government organisations, undertakings and government-funded organisations. It said that only non-proprietary software and open standards should be used for any government work. The declaration said that the University Grants Commission should ensure the usage of free software in the curriculum framework and research. The meet urged the governments to set up free software development centres. It also requested the Centre to issue a White Paper on the implications of the global financial crisis on the IT sector in the country.</em>
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  <em>The second national conference on free software that concluded at the Cochin University of Science and Technology (Cusat) on Sunday decided to initiate joint collaboration among six universities in Kerala, Andhra Pradesh and West Bengal to promote the use and expansion of free software. The meet also decided to set up a knowledge hub in Kerala based on free software. It will be managed by the Centre for Information Resources Management at Cusat. A declaration adopted at the conference said that faculty members in these universities would work jointly for encouraging research and development using the free software platform. The IT@School project of the Kerala government will promote its Vidyamitra project as an online educational system based on free software.</em>
</p>
