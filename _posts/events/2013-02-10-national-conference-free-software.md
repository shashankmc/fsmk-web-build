---
layout: post
title: "National Conference on Free Software"
date: 2013-02-10 10:43:06
category: events
---

National Conference on Free Software is going to happen in the month of March 2010 in Bangalore. FSMK is taking the responsibility of creating the canvas of the Conference. Delegates from different parts of India will be assembling here in Bangalore.

Read More on [National conference here][1].

 [1]: http://www.nc2010.fsmk.org
