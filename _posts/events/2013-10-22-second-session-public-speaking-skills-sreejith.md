---
layout: post
title: "Second Session on Public Speaking Skills by Sreejith"
date: 2013-10-22 08:28:55
category: events
---

In the previous session Sreejith sir gave us some assignments and asked us to practice it within a week (means within next class) with the help of the hints he gave us in that class.

The assignment was that, each and every students should select a different topic and give a speech of three minutes in front of the audience.The audience are not from outside and they were our own amigo community center friends and this gave us little bit peace to our mind.This assignment was given for us to get rid of the stage fear.But,unfortunately as there was enough time to practice, still we didn’t take it much serious and the time which Sreejith sir gave had also come(10-10-2013).Everyone were tensed and was preparing on that day only.But still everyone talked as there were some mistakes .But,it can be corrected.The crowd was about 17 to 20 same as the previous class.The topics selected by the students for the speech are as follows:

Divya                      -   ” Introduction of Karnataka”  
Monisha               -    “Environmental Pollution”  
Arshiya                  -     “How to become a good teacher?”  
Sujay                      -     “HIV”  
Kavitha                  -     “Todays Education System”  
Rukhaya               -     “Festivals celebrated in India”  
Bhavani                -     “About Pain(mental pain)”  
Ashwini                 -     “Gave an introduction of herself”  
Manoj Yadav       -      “Fuels”  
aa               

   Last but not least next was a speech given by our Saffeena it was same as what ashwini gave “Introduction of herself”. Though she made mistakes still she had the courage to talk in front of 17 plus members.This was the very good opportunity for us to know ourselves that who are we.And we were very happy by the speech given by saffeena and i personally was very much inspired by the courage that she had and i wanted to learn that kind of courage from her especially.Some of our friends didn’t get chance to give their speech.Because,of the time shortage.But,still Sreejith sir asked us to practice it regularly and he would meet us again when possible.

2013-10-03 18.38.11

-Divya.
