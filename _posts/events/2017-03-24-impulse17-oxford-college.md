---
title: Impulse'17
category: events
layout: post
date: 2017-03-24 00:09:00 +0530
startdate: 2017-04-07 00:08:30 +0530
enddate: 2017-04-21 00:06:30 +0530
discourse_topic_id: 562
---

TOGGLE and The Department of Computer Science and Engineering present 'IMPULSE'17' in association with FSMK.

TOGGLE(The Oxford Group of GNU/Linux Enthusiasts) is a GLUG established at The Oxford College of Engineering,under the guidance of FSMK to create awareness on free-software and hardware.

IMPULSE is a Technical Extravaganza,organised by students of The Department of Computer Science and Engineering every year at THE OXFORD COLLEGE OF ENGINEERING,Bangalore.
This time we have partnered with FSMK to organise a FEST by using FOSS tools.
The following events being mentored by FSMK , fully free-as-in-freedom:

**Coding:** A technical event wherein participants have to write code to solve the given scenario. Prerequisites are elementary coding knowledge in any language and a logical bend of mind.

**Web Design:** This event will feature a single challenge - design a webpage based on the given topic.

**Debate:** Defend your philosophy in this polarizing event.

**Tech-ads:** The next Orwelian advertisement could be yours. Try and redefine a generation of advertisements by writing them for your favorite FOSS technologies.

**KannaDinga:** Prove your mastery in the language of our ancestors. Help translate technical terms to the state's most loved language.

Be there to witness something extraordinary,one of its kind event where we promote FOSS at a bigger level.

Also it would be really great if you wish to volunteer and help us in making this event a grand success!

We expect the volunteers to participate in as many events as possible.

For more details Log on to <http://www.impulse2k17.in>
