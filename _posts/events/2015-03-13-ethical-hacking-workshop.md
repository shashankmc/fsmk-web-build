---
layout: post
title: "Ethical Hacking Workshop"
date: 2015-03-13 20:50:57
category: events
discourse_topic_id: 235
---

Hacking, the term by itself has acquired various connotations because of the unscrupulous use of it by the media. Hacking, simply is a means of refining, and in computer science of code and technology.  

To clarify this distinction two other terms - ethical hacking and cracking are used now.  

Ethical Hacking are the methods of using programming, computers and networks to refine the digital infrastructure. Hactivism is another facet of this technique.  

Free Software Movement- Karnataka, after taking up the Internet Censorship by protesting against the Intermediaries Bill, now is organising a hands on workshop on how to counter censorship and secure privacy in the Internet using a gamut of Free and Open Source tools.  

TOR, HTTPS, Firewalling, sockets, packet sniffing and more to acquaint users with the concepts of ethical hacking as applied to Internet Censorship is happening on the 15th of July.  

Date: 15th July  
Registration: Rs.100 per participant  
Time: 9:30 AM  

Venue: FSMK Office,  

(Opp. Old Harilakshmi Theatre)  
No. 121/17, 1st Floor, 6th main,  
14th Cross, Wilson garden,  
Bengaluru - 560030.  

We have seating capacity of only 25 people. Do confirm your participation in advance.   

For clarifications contact Raghavendra S (9986168349)
