---
layout: post
title: "FSMK Camp Fun. Freedom. Knowledge ; Registration is open"
date: 2014-05-21 16:04:39
category: events
---

We are back again , this time targeting 200 participants over a period of 9 days from July 19th to 27th.  Fore more detail <http://camp.fsmk.org/>

FSMK has been actively organizing workshops in various engineering colleges across the state since last 5 years. However, we noticed that a longer duration residential camp completely dedicated to Free Software technologies will be of much greater help to students as they will be able to interact more with the speakers and get lot of time to practice their skills. Hence in 2013, FSMK conducted 2 residential camps for students which saw huge participation. In Jan 2013 we conducted a 5 days residential camp at SVIT, Rajankunte which was attended by around 160 students. Later in July 2013, we conducted a 9 days residential camp at JVIT, Bidadi which was attended by around 180 students. Our camps are organized by volunteers from the students community and also industry professionals. Similarly our resource persons are industry professionals from companies like Cisco, SourceBits, SAP Labs, Intel, MobiSir, Arista Networks, Intuit, etc.  
**Speakers**

Free Software developers, Industry professionals, Academic experts and many more. We get our resource persons from various companies like Cisco, Intel, MindTree, Intuit, Arista Networks, Sourcebits, SAP Labs, Deeproot Linux, Mobisir. Also Free Software developers from communities like KDE, Firefox, Debian also join us to share their experiences.

** Technologies**

FSMK Camps are focussed on introducing students to various free software technologies and give them enough insight to get started with the technology whichever they are interested. The world is moving towards web, right from HTML5 apps to popular web applications. With the advent of javascript libraries like impress.js, even presentations are now being made using web technologies. Keeping this in mind, we have decided to keep the focus on latest Web related technologies. We have also learnt from previous experience that for effective learning, we should not switch between too many technologies. Based on this, we have decided to cover the following technologies.  
Please note that impromptu sessions if requested by students during the camp will be arranged on technologies which we are not covering in the main agenda based on the availability of the speakers.

**Sponsors**

Any individual, private or government institution interested in supporting FSMK in spreading Free software can help us by sponsoring the camp.  
The criteria to accept sponsorship is as follows:

*       Any educational institution, govt dept, public sector undertaking can be approached for sponsorship.
*       Any organisation, company, individual supporting/advocating the use-propagation of free software and its ideology.
*       Any IT company (free or proprietary) as long the product, service they are advertising with us is free software and does not violate the privacy of the service subscribers. Further, they may promote internships, job opportunities for the participating students.
*       Sponsors of services like media (radio, television, newspapers-magazines, internet), water, food, venue, accommodation.
*       Any well-wisher can contribute in terms of cash or kind.
