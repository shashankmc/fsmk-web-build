---
layout: post
title: "Walkathon in Bangalore  Save The Internet Protect Net Neutrality"
date: 2015-04-20 20:13:25
category: events
---

Walkathon in Bangalore - Koramangala - from National Games Village(NGV) gate till Forum.   
Date: 23 April 2015  
Meeting time 17:30 hrs.   
Bring your music, cosplay, placards, street plays...  
Please enter your contact details so that we may remind you about the events <a href="http://goo.gl/forms/vljdJu5ZHx" rel="nofollow" target="_blank">http://goo.gl/forms/vljdJu5ZHx</a>
