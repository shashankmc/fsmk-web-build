---
layout: post
title: "Teachers Workshop"
date: 2013-02-10 03:16:57
category: events
---

FSMK is jointly organizing Teachers workshop on Basic Computer aspects, free software, FS philosophy, Open source Tools, etc on July 16, 17, and 18 (10.30am to 5.30pm) along with other Free Software NGOs. Audience are school teachers from schools in and around Bangalore.

The syllabus is prepared by FSMK and is available in http://fosscomm.in/Resources/KarnatakaSchools/BaseSyllabus.

Venue  
4th floor,  
Office of the State Project Director,  
Sarva Siksha Abhiyan,  
Nrupathunga Road,  
Bangalore
