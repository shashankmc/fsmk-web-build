---
layout: post
title: "Copyleft@IISc :An Open Discussion on Danger of Patents"
date: 2013-02-09 17:41:33
category: events
---

**About the topic:**

In the wake of centenary celebrations of our Institute, whose aim is to empower the society through knowledge, it is time to ask “what are the issues that hamper  the creation of a free society where the fruits of knowledge could be equally Columbia Law School  shared”. Economic research shows that patents retard progress.

We will also discuss software patents as it’s a important issue in India right now.

Software patents are patents that cover software ideas. They restrict the development of software, so that every design decision brings a risk of getting sued. Patents in other fields restrict factories, but software patents restrict every computer user. The worst is when the patents are allowed to be trolled to foreign companies.

**About the speaker:**

Prof. Eben Moglen is a professor of law and legal history in Columbia University and the founding director of “Software freedom Law Centre”. He also serves as the director of “Public patent Foundation”. This “free software” advocate also referred to as a “hacking lawyer” started out as a programming language designer before he did his doctorate in law from Yale University in 1993. He is closely associated with the “Free software Foundation” and was responsible for enforcing the “General Public License” for GNU/Linux on behalf of FSF.


**Date**   : Sunday, 14th December 2008  
**Time**   :  4:00 pm           
**Venue** :  Open Air theatre (near Choksi Hall)
