---
layout: post
title: "First DevOps meet up in FSMK banner Events"
date: 2015-11-12 19:30:26
category: events
discourse_topic_id: 134
---

Venue  : [FSMK office, Wilson Garden][1]  
Date    : 15th Nov 2015  
Time    : 10:00 AM

 [1]: http://www.openstreetmap.org/node/2470953886

What is DevOps?  
DevOps is a software development method that emphasizes communication, collaboration, integration, automation. The method acknowledges the interdependence of software development, quality assurance (QA), and IT operations, and aims to help an organization rapidly produce software products and services and to improve operations performance.  
In traditional, functionally separated organizations there is rarely cross-departmental integration of these functions with IT operations. DevOps promotes a set of processes and methods for thinking about communication and collaboration between development, QA, and IT operations.

Agenda  
1. Introduction of members.  
2. Present the concept of devops meetup in fsmk banner.  
3. Tech talk on a devops technology. (Probably chef).  
4. Decide next activity.

 
