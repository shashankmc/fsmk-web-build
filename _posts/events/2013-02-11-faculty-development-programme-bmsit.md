---
layout: post
title: "Faculty Development Programme @ BMSIT"
date: 2013-02-11 17:40:37
category: events
---

<div dir="ltr" style="text-align: left;">
  <p>
    As sprint events to celebrate Software Freedom Day, and celebrating Software Freedom in its true spirit the second of the sessions in BMS Institute of Technology was organized on 8th September, 2011.
  </p>

  <p>
    After a session on the Free Software approach towards 'Privacy and Security in the Internet' for students, a full day workshop aimed at facilitating the faculty members of Electronics and Communication Engineering (ECE) Department of BMS Institute of Technology to migrate onto GNU/Linux platform with equivalent tools, to most of the ubiquitous tools used in the ECE academic scene were demonstrated.<br /> <br /> The overall response was overwhelming, and very positive. Being the ECE Department the speaker for the session Mr.Prabodh and myself was preparing to brace for a colder audience with some natural hesitation. But, on the contrary we found everyone really interested in the gamut of tools with undaunted enthusiasm till the end of the day. Numerous tools available for the various topics were demonstrated pertaining to the courses to be dealt teaching the curriculum. Through the day, interaction was highly productive with proactive queries and discussions.<br /> <span style="line-height:1.6; text-align:left">There is a pressing need for the academic institutions to migrate to Free Software, knowing the entire depth of positive ramifications of the tools, backed by the strong philosophy behind the tools. I am glad that my alma matter has taken this step. We, at FSMK are  certainly looking forward to sustained efforts to perpetuate the momentum instigated by today's sessions.</span>
  </p>

  <p>
    <span style="line-height:1.6; text-align:left">And talking specifically about Electronics related tools in the GNU/Linux domain, a special push is required to bundle these marvelous tools already available with comprehensive manuals. This way first smooth migration and prospective contributions can be hoped for.</span>
  </p> A big shout out to all who were part of today's workshop. Cheers!
</div>

-Raghavendra S
