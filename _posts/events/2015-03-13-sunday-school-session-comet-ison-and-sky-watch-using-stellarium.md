---
layout: post
title: "Sunday School  Session on Comet ISON and sky watch using Stellarium"
date: 2015-03-13 20:41:07
category: events
---

Comet C/2012 S1 (ISON) has been the most talked-about comet of 2013. When discovered in late 2012, it was said to have the potential to become a striking object visible to the eye alone around the time of its perihelion – or closest point to the sun – on November 28, 2013  

Stellarium is a free open source planetarium for your computer. It shows a realistic sky in 3D, just like what you see with the naked eye, binoculars or a telescope.

Sunday, November 3, 2013at 3:00pm - 5:00pm  
@FSMK Office
