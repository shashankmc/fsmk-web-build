---
title: Regional Camp Hassan is back
layout: post
category: events
date: 2017-03-13 18:30:00 +0530
startdate: 2017-04-13 09:00:00 +0530
enddate: 2017-04-16 17:00:00 +0530
discourse_topic_id: 550
---

FSMK camps are amazing. Where FSMK educates engineering students about free software philosophy and teaches competitive industry oriented technologies.

Participants from across the states attend these camps. But we always got feedback that seats are limited and also camps are Bengaluru centric. Due to which many enthusiastic participants from other parts of the state unable to attend camp.

To reach more and more audience FSMK started hosting regional non residential camps across state. As a result of this GLUGs from Hassan region (GLUE,RIT, NDRK,MCE) hosted a regional camp in 2016 at Govt Engineering College. Which was very successful with huge participation from Hassan regional engineering colleges.

With the huge success and inspiration from last year camp GLUGs from Hassan region are organizing regional camp this year again. Along with Free Software students are asked to teach Open Hardware technologies.

Below are some updates regarding regional camp till now.
Regional Camp 2K17 - Hassan

+ Tentative dates
13th - 16th April 2017

+ Free Software Track
    + Python and related frameworks
        Topics - pending
    + Speakers
        Not yet decided (Possibly @farhaan, Santhosh, @voidspace or from Python Express )

+ Open Hardware Track
    + Basics of electronics, Embedded programming, Arduino, IoT
        Complete schedule - [here](https://ethercalc.fsmk.org/yzjqlsr09d72)
    + Seakers
        @AnupPravin, pruthvi, @Kverma 

More to come!

Need support of all FSMK volunteer to make this a successful event.
