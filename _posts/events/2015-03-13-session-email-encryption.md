---
layout: post
title: "Session on Email Encryption"
date: 2015-03-13 20:12:38
category: events
---

Have you ever wondered how Google knows exactly where you are traveling and how it is able to show you targeted ads?  
Does it scare you that every email that you have on your email account is constantly being scanned and read to find something interesting about you.?  
Would you want to come out of this, in a very simple, easy way without spending a dime on complex solutions!   
Join us this Sunday at FSMK Sunday School at Wilson Garden at 10:30 AM.


By Vignesh Prabhu.  

Requirement:  
Please ensure that you have installed  
Thunderbird on your GNU/Llinux.  
After that , follow this tutorial for configuring your gmail account in thunderbird  

<a href="http://www.webupd8.org/2011/03/how-to-backup-gmail-using-thunderbird.html" rel="nofollow nofollow" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none;" target="_blank">http://www.webupd8.org/2011/03/how-to-backup-gmail-using-thunderbird.html</a>  

For Advanced user.  

Please also try following this article  
<a href="https://help.ubuntu.com/community/GnuPrivacyGuardHowto" rel="nofollow nofollow" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none;" target="_blank">https://help.ubuntu.com/community/GnuPrivacyGuardHowto</a>  
 

Sunday, August 24, 2014at 11:00am - 1:00pm

@FSMK Office,No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden, Bengaluru - 560030. Ph: 080-42817353
