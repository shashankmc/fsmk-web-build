---
layout: post
title:  General Body Meeting 2016
date:   2016-04-23 07:51 +0530
category: events
discourse_topic_id: 242
---


Hello! Greetings to all,

After thorough planning and rescheduling, FSMK has decided to hold its Annual General Body meeting on 24th April 2016, at FSMK Office, Wilson Garden,Bangalore. As part of this, we have come up with following procedure to get members from various sections of society to FSMK GB. General Body is the core group of FSMK which takes majority of the decision for the immediate direction of the movement. However our FSMK Discuss mailing list exists as an open group for much larger and wider audience to lead discussions for the movement in general. Importance of this General Body Meeting is that more FSMK activists will meet and engage in a detailed discussion on the various activities pursued by FSMK and its community, review of those activities, and finalizing action plan.

Mainly, we have 4 sections of society that we have identified: Students from our GLUGs, Academicians, Community Center members and Working professionals(non-academicians). From our understanding, this should cover more or less everybody associated with FSMK. For each of them, we have come up with a procedure to become part of FSMK General Body.

For GLUG members: GLUGs are requested to nominate delegates to the General Body. We suggest  the following process to make this effective:

1. How can GLUGs elect its delegates? Call a meeting of the GLUG members ie whoever are associated with free software activities. Brief them about the FSMK General Body meeting and its importance. Democratically decide who could be delegates to FSMK GB.

2. How many delegates a GLUG can elect? For every 10 GLUG members, 1 delegate may be elected by the students amongst who are gathered. If calling all the GLUG members for the meeting is very difficult at this point, atleast you call a meeting of the core GLUG members and finalize the names of the delegates to be sent to General body meeting. For example, if you have 50 GLUG members, you may elect/ finalize 5 delegates who can attend the FSMK GB meeting.

3. Where will the FSMK GB meeting happen?  At FSMK Office.

4. What is the role of GB member? A delegate who gets elected by the GLUG and attends FSMK GB will be included in the FSMK-Core mailing list. He/she would participate and work for building the free software activities not just in GLUG but at an organization level at FSMK. Such delegate will also have an opportunity to get elected further into the higher committees which will be decided in the FSMK GB meeting. He/she will also be responsible in ensuring that whatever gets discussed/decided in FSMK core is also passed on to the GLUG regularly.

For Community Center members: Since only 2 community centers are currently active, we have suggested them to nominate 5 members together from the same.

For Working professionals:  Since there is currently no unit as such to represent working professionals in FSMK currently, we have decided that any working professional interested to become part of General Body will have to pay Rs. 1000. Though this does sound as someone buying his/her way into FSMK General Body, we currently do not have any other better way for deciding who should be and who shouldn't be part of FSMK General Body from working professionals. We believe that the fee becomes a self-detterant to decide whether a person wants to commit himself/herself to become a General Body member and it will ensure that only those who see long term commitment to the movement will come forward  to pay the fees. We are open to any suggestions for rectifying this  process.

For Academicians:  As we do not yet have an active academicians chapter, we have decided to ask academicians also who want to become part of FSMK also to pay Rs. 10 to become part of General Body.

Please note that we consider the above plan as a guiding document for us to go ahead and organize a general body which will help us to re-group ourselves with more active members, come up with a more vibrant set of activists to lead the movement.

When (is the GB)?  On Sunday,24th April, 2016 from 10:30 to 1:00 IST

Where?  FSMK Office
 No: 121/17, Near Old Hari Lekshmi Theater 6th Main Road, 14th Cross, Wilson Garden, Bangalore - 560030

Thanks and Regards

 On behalf of FSMK
