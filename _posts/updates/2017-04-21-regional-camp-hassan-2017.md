---
title: Regional camp at Hassan 2017 - GLUEFRECA-2K17 and GEON ENRIFS-2k17
layout: post
date: 2017-04-21 11:45:00 +0530
category: updates
discourse_topic_id: 749
---

That was a successful completion of the Hassan Regional camp,The GLUEFRECA-2K17(software track) and GEON ENRIFS-2k17(hardware track)

Thanks for FSMK for providing such awesome speakers and having coordinated for the whole camp!!

Thanks for Ram and Navneethan for being speakers for the camp and Tanvi and Gunjan for volunteering.

Here's the complete details of the camp
<https://gluegech.wordpress.com/2017/04/17/first-blog-post/>
