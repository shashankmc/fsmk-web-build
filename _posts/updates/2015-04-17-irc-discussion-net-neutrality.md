---
layout: post
title: "IRC Discussion on NET Neutrality"
date: 2015-04-17 04:28:54
category: updates
---

In March 2015, Telecom Regulatory Authority of India (TRAI) released a formal consultation paper on Regulatory Framework for Over-the-top (OTT) services, seeking comments from the public. The consultation paper was criticised for being one sided and having confusing statements. It received condemnation from various politicians and Indian internet users. By 14 April 2015, over 300,000 emails had been sent to TRAI demanding net neutrality. As the last date for sending email to TRAI ends on 24th of April 2015 we plan to conduct series of activities across colleges and public spaces urging communities to take part in net neutrality campaign and build awareness among all.

As part of netneutrality campaign on April 18, 6 PM we have scheduled and IRC discussion on netneutrality topic on IRC: Freenode channel #SaveTheInternet. Join us.
