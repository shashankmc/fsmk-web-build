---
layout: post
title: "FreedomHack 2016 Summary"
date: 2016-01-11 20:33:00
category: updates
---

FSMK in association with Hacker Earth held a 24 hour long hackathon on Saturday and Sunday as a tribute to Aaron Swartz who lost his life in the fight to protect the openness of Internet and knowledge. This event is the first among a series of events that are planned around the theme #ForkTheInternet which is mainly intended to explore options of universalizing the Internet in a more secure manner.

The hackathon started on Saturday past noon with Santosh, the team lead,giving an introductory talk to the participants followed by Vikram explaining some of the complex ideas involved in fulfilling the goals. The hackathon had around 25 participants and volunteers/mentos helped them.

Jonas Smedegaard, Debian developer from Denmark, visited the hackathon, interacted with the participants and helped critique their work. Jonas is also one of the initial contributors to freedombox software.  Thefreedombox is a hand-sized server running Debian GNU/Linux which is meant to facilitate free, safe and secure communication in areas which do not have access to stable network, areas of political unrest and areas where privacy and freedom are restricted.

[Photos][1]

 [1]: https://www.flickr.com/photos/124434814@N02/sets/72157662773150570/
