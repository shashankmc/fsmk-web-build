---
layout: post
title: "FSMK with Chennai"
date: 2015-12-04 19:21:42
category: updates
---

Dear Friends,

As all of you would have heard, the northern coastal districts of Tamil Nadu have been severely affected by flooding in the aftermath of unprecedented rains. Thousands have been rendered homeless and many more have lost their livelihood. Transport and communication systems are paralyzed.

A massive effort is required to help the thousands of stranded people get back on their feet. Our sister organization in Tamil Nadu Free Software Foundation Tamilnadu (FSFTN) is also involved in relief work. [FSFTN ][1]volunteers have already been active in distributing food to areas around  T.Nagar.

 [1]: http://fsftn.org/blog/call-for-flood-relief-contributions/

It’s our social duty  to support and help our brothers and sisters in Chennai and northern districts of Tamil Nadu. We are seeking your help in providing support to the victims. Thousands of people are stranded in different parts of city with little access to food or other basic necessities. FSMK is requesting you all to contribute to help Chennai to come out of this natural calamities.  The funds and materials collected will be passed on  to FSFTN, our sister organization who are in ground and helping victims.       

       
If you wish to send money , please find the account details below.

Account Number : 352102010049659  
Account Name : Free Software Foundation Tamil Nadu  
Bank : Union Bank of India  
Branch : Thyagaraja Nagar, Chennai  
IFSC code : UBIN0535214

If you wish to contribute relief material, please contact

Suthir - 9944384857/7358061140  
Vinoth - 9789188900  
Raghu - 95910 42333  
Shijil  - 8892324346  
FSFTN office number - 04443504670
