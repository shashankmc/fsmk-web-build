---
layout: post
title: "FSMK Summer Camp 2015 Volunteer Registration and Selection Process"
date: 2015-05-04 18:33:48
category: updates
---

FSMK Summer Camp 2015 - Volunteer Registration and Selection Process  

1. GLUG Coordinators can share the registration form to its core team.  

2. Registration will also be open to non GLUG Members.  

3. Interested Students who want to volunteer should register on or before May 10th.  

4. Volunteer Registration Responses Review process - May 11th  

5. Shortlisting of Volunteers for FSMK Summer Camp '15 based on below mentioned points:

*   Active GLUG Member.
*   Should be available for all pre-camp activities
*   Should Attend Volunteer training program
*   Should be available for all 7 days during camp.

7. We shall announce the shortlisted Volunteers name on May 14th.  

8. Allocation of volunteers to each stream(Mobile App/Networking/Free Hardware) will be based on the knowlwdge level , and this will be done collectively after volunteer workshop.  


Register here: :point_down:  
<a href="http://tinyurl.com/camp15volunteers" target="_blank">tinyurl.com/camp15volunteers</a>

For more detail on camp

http://fsmk.wikia.com/wiki/Summer\_Camp\_15  

For further detail, contact  
Rakesh - 7204044233  
Shijil - 8892324346
