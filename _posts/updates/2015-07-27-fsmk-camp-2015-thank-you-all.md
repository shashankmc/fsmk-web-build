---
layout: post
title: "FSMK Camp 2015 Thank you all"
date: 2015-07-27 09:58:00
category: updates
---

<div>
  <pre>
Some keywords we observed during the feedback given by students are
 “amazing”,  “opportunity”,  “awesome”, etc. It is really heartening to see
that FSMK is able to live up to the expectations of the Camp15
participants. FSMK has started its big scale activities back in 2010.  We
are thankful to many individuals, organizations and Reva in making this
event a successful one.

First activity in Reva was a faculty development program. After that FSMK
has been conducting summer camps and winter camps in different colleges in
the city. This is the fourth summer camp that fsmk conducted.

1. Chairman and chancellor of Reva, Dr. P. Shyamaraju
    It was one of the best infrastructure that we have got for conducting
the summer camps. Thanks to the chairman and management in letting us
conduct this years camp in Reva University.

2. Principal Director of Reva, Dr. S. Y. Kulkarni and Principal of
Reva, Dr. Sunil kumar S Manvi.
   They have been a constant inspiration for us in conducting the events.

4. Faculty coordinator Gopal Sir and Prof. Albur Sir
       Worked constantly as a bridge between Reva University and FSMK in
the smooth execution of the camp.

5. Manjunath and Krishnappa,  lab in charges and MuniSwami System admin at
REVA
    They have been really supportive and flexible through out the camp.

6. Communities/Startups supported us in the camp
    a. OSM
    b. Jnaapti

7. Other Resources
    a. Svaksha
    b. Pavanaja U. B -CIS
    c. Muni Reddi -IBM

6. Team FSMK.

    Prof. Gopinath, Vikram, Jay, Shijil, Rakesh, Sarath, Vignesh, Divya,
Shashank, Nikhitha, Jeeva, Hariprasad, Raghuram and  Rameez

    Most of them were relentlessly working for the camp for last few
months. Preparation of syllabus, finding out resource persons, venue, food,
accommodation,  hardware kits, T-Shirts, stickers, designs, website, etc
were few tasks coordinated by this team.

7. Volunteers.

    Camp without these young volunteers is unimaginable. We just created a
framework and they built it as a platform. There were about 50 volunteers
this time.
    Special thanks to Farhaan, Kishan, Rahul Kondi, Vishal, Jeeva, Suryo,
Ramya, who stepped up as resource persons for taking sessions.
    Another special thanks to the website and design team for making an
awesome website, design for our posters, stickers, t-shirt.
    Thanks to kishan for creating the camp distro and managing the camp
servers.
    Thanks a lot to FSFTN volunteers Sharath, Naveen, and Karthik for
helping us in the free hardware stream.

8. Participants.

We are looking forward to you. You are our flag bearers. I am sure you
distinguish us from any other training institutes or groups. First of all
it I not a training institute. This is a movement. Momentum and Expansion
of the movement is directly proportional to the passion and number of
activities that you do in the lines of this organization. Our plan is do to
more regional camps in the coming year. Your participation on such camps
are mandatory for the success of this agenda.

Thank you for staying away from the comfort of your homes and trusting us
to come this long way.

Thanks one and all for working towards the success of this camp. see you
all again in the upcoming events of fsmk.

PS : This note is prepared and delivered by Rameez on the occasion of
valedictory program. I added few more names missed then. Yet I might have
missed few. However once again thanking all for all your effort and
commitment for making this camp successful.


With Regards
Team SummerCamp 15
</pre>
</div>
