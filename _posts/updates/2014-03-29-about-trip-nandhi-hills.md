---
layout: post
title: "About trip to NANDHI HILLS"
date: 2014-03-29 13:12:29
category: updates
---

Hello Amigos,after so long iam writing an article.From the day we started our Amigo Community Center(ACC),we haven't go for any trip exceptional for which were conducted by FSMK.For example, SWATAHA,Savandurga and so on.These places are completely not went for trip.But it involves some fest,camps,etc.,we have been planned several time to go for a trip.But,we didn't get exact time for that or we really don't know that why we were unable to go for a trip.Atlast we thought we should not miss this time and discussed together and finially we went for a one day trip to NANDI HILLS with the strength of 18 members.The means of transport was public transport system.

[<img alt="IMG_0033" class="alignnone size-medium wp-image-144" src="http://divyats75.files.wordpress.com/2014/03/img_0033.jpg?w=300" style="height:225px; width:300px" />][1]           [<img alt="IMG_0046" class="alignnone size-medium wp-image-145" src="http://divyats75.files.wordpress.com/2014/03/img_0046.jpg?w=300" style="height:225px; width:300px" />][2]

 [1]: http://divyats75.files.wordpress.com/2014/03/img_0033.jpg
 [2]: http://divyats75.files.wordpress.com/2014/03/img_0046.jpg

All of us were asked to bring RS.200 individually for tickets and other things.We went on 27th of October 2013 on sunday.We left at 9 A.M form FSMK office if i correctly remember.The trip was a very good experience for all of us.Because, this is the first time for our ACC members to organised the trip by ourselves.And,this was successfull attempt for us is what i personally hope so.Before reaching NANDI HILLS,we had our breakfast in a place.With the help of the carriers we carried the food.The food was prepared by Dharmendra sir and Manjunath sir's mother.The dishes were puliyogare and mosaranna (tamrind rice and curd rice).

[<img alt="IMG_0061" class="alignnone wp-image-137" src="http://divyats75.files.wordpress.com/2014/03/img_0061.jpg?w=300" style="height:249px; width:322px" />][3][                 <img alt="IMG_0064" class="alignnone wp-image-138" src="http://divyats75.files.wordpress.com/2014/03/img_0064.jpg?w=300" style="height:250px; width:311px" />][4]

 [3]: http://divyats75.files.wordpress.com/2014/03/img_0061.jpg
 [4]: http://divyats75.files.wordpress.com/2014/03/img_0064.jpg

After reaching Nandhi hills we were taking some photos-snaps and we were seeing all the sceneries.It was a very good place for sceneries.Then some of us went to the temple which was nearby,when the others remined in the same place.Then,we all together had lunch and was playing there for a while.

<img alt="IMG_0130" class="alignnone wp-image-136" src="http://divyats75.files.wordpress.com/2014/03/img_0130.jpg?w=300" style="height:225px; width:337px" />     [<img alt="IMG_0110" class="alignnone size-medium wp-image-143" src="http://divyats75.files.wordpress.com/2014/03/img_0110.jpg?w=300" style="height:225px; width:300px" />][5]

 [5]: http://divyats75.files.wordpress.com/2014/03/img_0110.jpg

While having lunch,as you know our thickest and reliable and best friends,MONKEYS were not living us to have the food.They were angry on us for we were having without inviting them for lunch.Some of us were holding the sticks to shoo off the monkeys,

[<img alt="IMG_0227" class="alignnone size-medium wp-image-139" src="http://divyats75.files.wordpress.com/2014/03/img_0227.jpg?w=225" style="height:300px; width:225px" />][6]        [<img alt="IMG_0215" class="alignnone wp-image-140" src="http://divyats75.files.wordpress.com/2014/03/img_0215.jpg?w=300" style="height:295px; width:446px" />][7]

 [6]: http://divyats75.files.wordpress.com/2014/03/img_0227.jpg
 [7]: http://divyats75.files.wordpress.com/2014/03/img_0215.jpg

while others were having food.Atlast we were playing a group game which is helpfull in team building.THe game is 'Indian Chief'.

[<img alt="DSCN0363" class="alignnone size-medium wp-image-141" src="http://divyats75.files.wordpress.com/2014/03/dscn0363.jpg?w=300" style="height:225px; width:300px" />][8]

 [8]: http://divyats75.files.wordpress.com/2014/03/dscn0363.jpg

It was really enjoyfull and we had a lots of fun.And,at that time while we were playing games a couple of english people came there and took a video of us,said bye and went off.Then,it was already time to reach home.So, we left form there.Last but not least.I said that 18 members were part of this trip.But,i didn't mention their names there were my selfss,Rameez chetta,Kavitha,ACC Karthik anna,Sujay,Meezan,Shijil,Arshiya akka,Shamsiya akka,Rukhaya,Hussian,Saffeena,Chaithra,Divya shree,Dharmendra sir,Manjunath sir,Hemavathi ma'am,Vignesh and Arvind.

<img alt="IMG_0258" class="alignnone wp-image-135" src="http://divyats75.files.wordpress.com/2014/03/img_0258.jpg?w=300" style="height:230px; width:307px" />     [<img alt="DSCN0256" class="alignnone size-medium wp-image-142" src="http://divyats75.files.wordpress.com/2014/03/dscn0256.jpg?w=300" style="height:225px; width:300px" />][9]

 [9]: http://divyats75.files.wordpress.com/2014/03/dscn0256.jpg
