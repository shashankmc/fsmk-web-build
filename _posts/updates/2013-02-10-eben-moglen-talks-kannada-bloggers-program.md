---
layout: post
title: "Eben Moglen talks for Kannada Blogger's Program"
date: 2013-02-10 01:54:58
category: updates
---

3 December 2008, evening Eben talked at Institute of Agriculture Technologies on the subject 'Free Software and New Media' and had informal discussions with Kannada bloggers and Media persons.

Eben had an informal chat with people around him. People asked wide range of question from philosophy, law, Free Software, media et al. His answers were pedantic.  
There were questions from philosophy, It's observed that a weak person prone to make mistakes when these mistakes affect others it's considered as crime. An individual is weak for many reasons, one of such reasons that the society itself makes individual as weak citizen. What is your comment? He answered, I put in nutshell, Every individual has a responsibility towards other people. When one fails to execute his responsibility it affects another person then next then next and continues and may result in a crime or mistake. When it's a crime the court of law will NOT judge as an ordinary people jury will go according to the book which is insensitive to human emotions. Now the power is with the state NOT with the ordinary people. If a common man has to be judged by the people with ordinary emotions then the power should be with ordinary people NOT with the state.

Professor Eben Moglen, professor of law and legal history at Columbia University and is the founder, Director-Counsel and Chairman of Software Freedom Law Center, spoke on the topic free software in new media on last Saturday 13th December at the Institute of Agricultural technologists, Bangalore. The session was organized by Kannada bloggers and free software supporters in Bangalore. There was an audience of around 80 people. Apart from Professor Eben Moglan, Jayakumar (Kannada Blogger & Volunteer of Free Software Activist), and HP Nadig from sampada.net also spoke at the event.

“The free software and the emerging new media is reducing the significance of the conventional media where the communication was only one way. Wikis are allowing people to share and collaborate knowledge and thus allowing interaction and direct communication. Eben explained how he use Wiki to teach students and the way it improves the interaction between students and teachers and how it helps to improve teaching. It is emerging as an alternative to the traditional examination system. He also explained an instance where the Wiki helped a researcher from Europe to collaborate with students in Columbia University.


He quoted the Wikipedia model where people write content and collaborate to improve it rather than the traditional model where the editors had a significant role in choosing the content. In the Wikipedia way, people collectively acts as watch dogs to check the accuracy of content. While talking about blogs, Eben said that blogs provide a platform for users and freedom to express. When bloggers express and share the knowledge they gained through vast reading and research through blogs, it benefits the society in a significant way.

He also added that the w3c consortium is working on including audio and video files in HTML 5.0 in such a way that a user can use them using a browser without downloading any plug-ins. You would be able to include multimedia files just using HTML tags like <audio> and <video>. He emphasized that the major obstruction to this was not the difficulty in using or developing the technology, but the patents and the copyrights which were making it hard to include those new advancements. The major obstruction was the proprietary use of multi media codecs.Once codecs are available, it will revolutionize the way video is transmitted through the web. Then we don't need television as people will express content through multi media and everyone will be able to watch and listen to the content without any plug-ins. The free software and the new media is putting every one in the drivers seat from the position of mere spectators or users.

After the event, Eben Moglen interacted with the audience and clarified their queries.  
While answering the questions about the significance of free software Eben Moglen emphasized that profit is not the only human tendency. There are also other tendencies like sharing and collaboration that helped human beings to emerge as a society. The tendency to help is one of the basic natures of human beings. There are a lot of instances in everyday life where we help someone without expecting anything. Free software helps us to make a better society.”

FSMK member and Kannada Blogger, Likith expressed the vote of thanks for concluding the event.
