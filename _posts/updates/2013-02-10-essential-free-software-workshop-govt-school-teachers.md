---
layout: post
title: "The Essential Free Software Workshop for Govt School Teachers"
date: 2013-02-10 05:19:20
category: updates
---

Write-up By Raghavendra S

We,the FSMK(Free Software Movement Karnataka) volunteers cum trainers, along with members from few other organizations trained Government School teachers to use and get comfortable with Free Software,in a step towards scaling a steep task of setting the binary knowledge free. These training sessions happened on the 16th, 17th and 18th of July,2009.

I was expecting reluctance,hesitation,mild levels of ego and confusion from the participants...But all that emerged out of these teachers was an extremely high level of enthusiasm, an equaling curiosity and tireless efforts for the first two days...Witnessing this in person and being a part all three days has been all together a new kind of an experience....

We were able to break the dogma of The Cyberphobia (Fear of working with computers) from these people the very first day....The joy they subsequently found working on the basics which we taught them was just heart warming.... The ultimate goal of this workshop has been making the teachers self sufficient in handling computers for basic purposes, so that the kids are benefited.

The second day was more about trying to make them feel more independent. The interest and the curiosity levels in the teachers remained still the same, while the extent of apprehension and inhibitions from their side were mellowing down.

The third day was to introduce them to an entirely new world, as seen by them, through Internet and the various possibilities using it like mailing, searching, browsing useful sites, copying content from net, file downloads etc... The Internet really hooked them on,making most of them pledge that they would want to use it more even after the sessions.

But,the kind of awe they were in, while undergoing these sessions puts a big question about the IT revolution India is supposedly going through...In a city where all the major IT players of the world have at least one establishment is ridiculously unbalanced. Its own teachers are literally scared to work with computers... Blaming the Government is something which is eternal...And we've got used to it...

The question is what each of us can do? I genuinely feel that we as a literate community have a lot to do with a sense of sincere concern towards the society we are a part of....

We at FSMK are striving to promote Free Software,which is a matter of the users' freedom to run, copy, distribute, study, change and improve the software. Free software is a matter of liberty, not price. To understand the concept, you should think of free as in free speech, not as in a freebie. FSMK functions by growing this awareness in the relevant sectors of society mainly by motivating students to volunteer. This is being realized by forming GNU Linux Users Groups or Glugs in educational institutions.

If you feel that,you can play a part in setting knowledge free,and would want the formation of a Glug in your institution,visit us at fsmk.org ,subscribe to our mailing list - fsmk-discuss@lists.fsmk.in and write to us.
