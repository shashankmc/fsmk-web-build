---
title:  L10N Sprint 1 at IISc Open Day 2017
category: updates
layout: post
date: 2017-03-05 00:09:00 +0530
discourse_topic_id: 530
image: l10nsprint_iisc_open_day_2017.png
---

ನಮಸ್ಕಾರ

This is a report on FSMK's activity around Localisation at IISc Open Day 2017.

What is L10NSPRINT?
A Localisation Sprint is a hands-on event where a group of people come together to participate in localisation activities.

How was this sprint conducted?
We set up a desk exclusively for Localisation activities. We made a list of English strings used on the new fsmk.org website. We invited people to help us translate these strings to Kannada. Anybody who translated more than 5 strings satisfactorily was given a Distro. Option included old Ubuntu CDs, FSMKCamps Distro CDs, EduBOSS Linux CD and Namma Debian CDs (they may have been DVDs.) There were also some stickers to give away.

Results?
We found some essential translations that we had missed out on in the localisation efforts we made a couple weeks prior to this event. We even got some better alternatives for strings we had translated.

Bonus
We had some of the people translate the handouts we, err, handed out to everyone. I will work on making Kannada versions of our handouts and post soon.

Volunteers who made this happen
Arun
Aishwarya
Akshatha
Rahul
And a lot of people from other desks. You know who you are. : )
