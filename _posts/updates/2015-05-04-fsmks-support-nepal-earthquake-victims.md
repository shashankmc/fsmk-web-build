---
layout: post
title: "FSMK's support to Nepal Earthquake Victims"
date: 2015-05-04 03:51:36
category: updates
---

Natural disasters are reminder of how fragile human life is on the planet earth. It is also an event where  
humans come together forgetting all their differences of caste, creed, religion and nation. People across the  
world are coming out to support victims of the dastardly earthquake at Nepal which has caused loss of life of  
close to 6500 people and rendered lakhs of people homeless. FSMK has also decided to mobilize its  
students through GLUGs to collect relief materials for the earthquake victims.

GLUG members are encouraged to collect relief materials by campaigning in the college on any convenient  
day and also spread the news through on-line and off-line medium. GLUGs can reach out to the students  
who are from Nepal in their college and work with them to collect relief materials. This will also help us in  
connecting with them for future activities.  
All the collected material will be consolidated on May 10 th after which it will be sent to Nepal. The focus is to  
collect the following materials, instead of funds:

*    Medicines
*    Food
*    Blankets
*    Cloths

We are also in touch with NepaleeseInBangalore group which has already done such campaigns in other  
colleges and sent relief material to Nepal. We may take their help to transport the material.  
Nishchal from FOCUS, the GLUG at SVCE will coordinate this effort in Bangalore and Chandan Goopta, an  
alumni of FSMK currently in Kathmandu University will coordinate it in Nepal. We would encourage other  
students also to come forward to take responsibility in this and help us collect as much relief material as  
possible.  
Please do contact the following in case you have any queries:  
1. Shijil - +918892324346  
2. Nishchal - +918904265492  
Let us try our best to help the victims of the Nepal earthquake.


Regards,  
Vikram Vincent,  
General Secretary,  
FSMK
