---
layout: post
title: "FSMK Newsletter Volume 1"
date: 2013-04-08 17:48:39
category: updates
---

Dear all,

We welcome all the members, sympathizers and critics of FSMK to the first edition of our News Letter. This is a medium where we can share our thoughts in technical, philosophical and free software politics in its depth and breadth.

  
We primarily focus on "Free Knowledge" and anything concerned to it. Our scientific understanding is that only a 'mass movement' in the Free Software domain can bring about the various changes that we envision. In brief this newsletter is going to publish articles, content that would help in reaching the above target. We would like to raise debates, conduct discussions through this medium to come to a consensus in building a knowledge base society.

We appeal to our readers to support and join hands in our journey achieve this. Articles, news items, debates discussions are invited from people. You can mail your suggestions, comments, articles to editor@fsmk.org.

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK Newsletter Volume 01.pdf" target="_blank">FSMK News Issue 01</a>

Thanks  
Editor FSMK
