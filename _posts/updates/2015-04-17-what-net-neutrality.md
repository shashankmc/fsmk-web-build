---
layout: post
title: "What is Net Neutrality ?"
date: 2015-04-17 04:01:58
category: updates
---

Net Neutrality is a term which embodies the principle that all data on the internet must be treated equally. What exactly do we mean by treated equally you might ask. Currently on your broadband plan you can access any website in the world that you want.

In the absence of net neutrality you would have packages of sorts, just like your d2h companies offer. For various prices you get to access different websites.

For example:

For a basic 500 rupees you can access certain websites as determined by your ISP. If you then want to access websites in package 2, which we’ll say includes websites like search engines, Youtube and Blogs. You are given this access for another 200 rupees.

But do you now want access to news websites as well? You can get it. For another 100 rupees though.

This is essentially what the absence of net neutrality entails. Currently with your internet plan, you pay a certain sum say 1000 rupees, but you can access any website you choose without having to pay extra or shifting plans. You don’t need to seek your ISPs permission to visit these pages. Without net neutrality, a system of pay for what you use emerges.

*   But really is that a bad thing?

Most agree this would be an unfavourable outcome, given your ISP now controls which website you can access from your device. But there are a few who argue, that it may bring down broadband costs for the user who may only want access to certain sites. Maybe instead of a 1000 rupee all-encompassing plan, you now pay 800 rupees for access to search engines and social networks. The other argument against net neutrality is that ISPs have to spend millions of dollars on developing the network infrastructure( laying miles of expensive optical fibres and other infrastructure), and like any investment, one expects returns. But the way it works today, the greatest benefit out of their investment goes to what are called “Over the top players”. These include websites like Youtube and NetFlix. The investment by telecom companies in their networks helps increase the profits of these OTTPs who can now offer faster and wider services but who also at the same time earn significant revenue from advertising and data mining. Doesn’t seem too fair does it? For Airtel or Vodafone to spend millions on cables and licenses and maybe get a return of 5-10 %, while companies like Google and Facebook rake in billions of dollars at no cost at all.

*   So what does this mean to you, as an average internet user?

Unfortunately we find ourselves stuck between a rock and a hard place. On one hand you have your ISPs trying to increase their revenues by selectively charging for each website you access ( Just think about how many websites you access in a day, imagine having to pay 50 rupees a month for access to every such website. Won’t that make you think twice before clicking on that funny video your friend just shared on your wall?) and on the other side, OTTPs like Whatsapp and Skype making billions of dollars out of selling ads on your webspace.

*   Okay, so is there anything else I need to know about net neutrality?

Well, yes. We talked about how there is differential pricing for websites. But this can be done in one of two ways. Either the ISPs charge ,you, the user, to give access to these websites.

Or the alternative is that the ISPs charge the websites directly to grant their customers access to their websites.


 

The second option doesn’t seem so bad, now does it? If youtube wants my views, let them pay for it. We all know they can.

*   Doesn’t that solve the net neutrality debate? Let the deep pockets pay!

Not really. Ever wondered why so many of these tech start ups begin in the Silicon Valley? One of many reasons is that the legal environment in California encouraged the growth of start ups. So now go back in time about 10 years, Facebook, that site with which we can’t go an hour without, is now a whacky idea in some Harvard dorm room.

*   What if Mr. Zuckerberg now has to pay Airtel or Vodafone millions of dollars to get access to you, the ISPs customers?

He like many other fledgling startups cannot afford to do so. Hundreds of ideas like Facebook or maybe even better ones, are now nipped in the bud. To put it in terms of India. Sure, Ebay can afford to pay Airtel a few million every year for access. But what about this new start up called Flipkart? Net Neutrality puts every website or company on an equal footing, regardless of how much money they have. You get access to them all equally. This is the reason fledgling companies like Flipkart have been able to take on giants like Ebay and establish themselves. Imagine what it would be like without Facebook, Youtube, Flipkart and Whatsapp because they couldn’t afford to pay airtel millions.


 

*    Wait. You didn’t say anything about Whatsapp? What about other apps?

Oh yeah, even apps can be affected. We saw an example of this a few months ago. Airtel decides to charge 16 times more for using Whatsapp or Skype. That scared us.

*   Why charge such a high price for whatsapp though?

Whatsapp is also classified as an OTTP. It’s the same reasoning, Airtel pays thousands of crores to buy 3g licenses and spectrum at government auctions, and Whatsapp and Skype just rake in the money.

*   So what is it that we can do? How is Consilience dealing with this issue?

We do what every citizen of a democracy does. We make ourselves heard. We know this is effective. When Airtel decided to charge 16 times more for Whatsapp and Skype, the social networks emerged in a roar of protest. Millions of people were now talking about how this was bad for us and seeing the uproar within four days, Airtel cancelled its plans. But like we’ve mentioned before, Airtel isn’t doing that just because it’s a profit hungry company, its because they spend so much money on giving us faster and wider internet access, its only fair that they get paid.

Consilience is going to act as a platform for these two sides to meet and engage in discussions on the best way forward and how to solve these problems. It’s imperative for there to be debate and discussion on this issue. You know why? It’s quite scary actually.

Courtsey : National Law School of India University.

 
