---
layout: post
title: "FSMK Weekly Report"
date: 2014-08-17 12:14:45
category: updates
---

Upcoming Activities scheduled as on today

*PACE GLUG Inauguration on 18th Aug

#PYMONTH

Glug DSCE conducts Python workshop on Aug 30th/31st  
Glug SVCE conducts Python workshop on Aug 23rd/24th  
Glug PACE conducts Python workshop on Sept 6th/7th  
Glug TFSC conducts Python workshop on Aug 30th/31st

Past Activities

#FSMKSC14S

Another camp from FSMK has completed on (27/July/2014). 9 days, 200  
participants and 30 volunteers. Dayananda Sagar college near  
Banashankari was the venue this time. Software Freedom Law Center  
(SFLC) has sponsored the event.  In addition to the paid participants  
there were about 15 students with scholarship also joined us in the  
camp this time.  3 months long preparation for the camp has culminated  
in creating some inspired camp participants who echoed the need of  
GLUG in their campuses and more activities.  They understand the need  
of freedom in the space of software and technology and net neutrality.

More Read :  
<http://ssrameez.wordpress.com/2014/07/29/fsmk-summer-monsoon-camp14/>

Other blog posts from students

<http://nitinr92.wordpress.com/2014/07/20/fsmk-sc-14-the-initial-set-up/>  
<http://konetipoojitha.wordpress.com/2014/07/28/fsmk-summer-camp-14-2/>  
[http://chandhu455.wordpress.com/2014/07/29/learning-could-have-never-bee...][1]  
<http://ssrameez.wordpress.com/2014/07/29/fsmk-summer-monsoon-camp14/>  
<http://goo.gl/huUnJA>  
<http://yourstory.com/2014/07/fsmk-freedom-fighters/>  
<http://kiran1707.wordpress.com/2014/07/29/i-was-there-and-we-were-there/>  
<http://blogchetu.wordpress.com/2014/07/30/free-as-in-freedom/>  
<http://farhaanbukhsh.wordpress.com/2014/07/30/hactivism-in-progress/>  
<http://dhireshajain.wordpress.com/2014/07/30/fsmk-camp-14-07/>  
<http://vineet-y-blogs.blogspot.in/2014/07/fsmk-summer-camp-1407.html>  
[http://fsmk2k14myfirstblog.blogspot.in/2014/07/my-experience-at-fsmk-was...][2]  
[http://nitinr92.wordpress.com/2014/07/30/when-fsmk-brought-order-from-ch...][3]  
<http://mahesh94.wordpress.com/2014/07/30/fsmk-party/>  
<http://abhilashkashyap.wordpress.com/2014/07/30/the-fun-with-freedom/>  
<http://shruthibhat.wordpress.com>  
<http://maheshkumar21.wordpress.com/2014/07/31/start-to-end/>  
<http://varunraj0011.wordpress.com/2014/07/30/200-free-birds/>  
<https://spokenthoughts9.wordpress.com/2014/07/30/fsmk-camp-2014/>  
[http://amrutharamanathan.wordpress.com/2014/07/31/and-my-perspective-cha...][4]  
<http://jnaapti.com/blog/2014/07/31/jnaaptis-day-out-fsmk-2014/>  
<https://abhishekjfsmk2014.wordpress.com/>  
<http://v41shr40.wordpress.com/2014/07/28/boot-camp-dsit-2014/>  
[http://samruda94.wordpress.com/2014/07/27/fsmk-summer-camp%E0%B2%A8%E0%B...][5]  
<http://saikiranhegde0793.wordpress.com/>  
<http://wp.me/p4SifT-3>

 [1]: http://chandhu455.wordpress.com/2014/07/29/learning-could-have-never-been-such-a-fun/
 [2]: http://fsmk2k14myfirstblog.blogspot.in/2014/07/my-experience-at-fsmk-was-fabulous-i.html
 [3]: http://nitinr92.wordpress.com/2014/07/30/when-fsmk-brought-order-from-chaos-14-07/
 [4]: http://amrutharamanathan.wordpress.com/2014/07/31/and-my-perspective-changed/
 [5]: http://samruda94.wordpress.com/2014/07/27/fsmk-summer-camp%E0%B2%A8%E0%B2%B2%E0%B3%8D%E0%B2%B2%E0%B2%BF-%E0%B2%A8%E0%B2%A8%E0%B3%8D%E0%B2%A8-%E0%B2%85%E0%B2%A8%E0%B3%81%E0%B2%AD%E0%B2%B5/

Post camp activities

#SUNDAYSCHOOL

Conducted Session on Free Hardware  16th Aug :-  
Discussion and demonstration of automatic doorbell for home  
automation. Also covering, demonstration of 'Line Follower'  
Handles by Shahsank

Conducted Session on AngularJS and Algorithms 10th Aug:-  
Building Web Apps using AngularJS and Algorithms group's first  
meet-up: Divide and Conquer Algorithms-1  
Handled by Karthik Rao and Abhiram  
<http://fsmk.org/?q=sunday-school-angularJS>

Conducted Session on Git (Basics + Advanced) 3rd Aug :-  
What's and Why's of Git and a few basic commands to get started.  
Advanced commands with a practical exercise of collective  
collaboration over a network to highlight the advanced features of Git.  
Handled by Darshan, Kishan and Karthik

#PLUGIN  
Plugin Inaugural Session On 13th Aug at PESIT  
A great introductory session to FOSS and GLUGs. I’m sure we all know  
that the ‘free’ in free software means ‘freedom’ by now. :P The  
volunteers did a great job finding relevant information on various  
topics like FOSS, Linux, Open Hardware, Open Source Communities. And  
getting the right people together like the senior faculty member Anand  
Sir and the FSMK core member Raghuram...

More Read  
<http://pluginatpes.wordpress.com/2014/08/13/plugin-inaugural-session/>

#GLUGDSCE

Introductory Session 9th Aug at DSCE

The Activities of GLUG DSCE for this Semester kick started with an  
Introductory session on Free Software and Linux which was conducted by  
RAMYA, who is also a core member of GLUG DSCE, on 9th August, at the  
IBM Centre of Excellence, DSCE. The event started at 1:30 pm and it  
was attended by students of our college and the core members of GLUG DSCE.

For More read  
<http://glugdsce.wordpress.com/2014/08/09/report-on-introductory-session/>

Html workshop - 16th August 2014

A handson session on html 5 was conducted by glug-dsce on 16th august  
in the IBM center of excellence lab of ISE department of the college .  
The session was attended by 75 students of 3rd , 5th semester of ISE  
and CSE department . The session was precided by Vishal verma and  
Sagar hani , who are 5th sem students of ISE department of the college.

For More read  
<http://glugdsce.wordpress.com/2014/08/16/report-on-html-session/>

#TFSCGLUG

BMSIT Glug First session of The Free Software Club Friday, 15 August 2014

After the camp, the volunteers of Fsmk decided to make the Bmsit Glug  
more active from this sem and spread about the Free Software to more  
people out there. The Free Software Club (TFSC), also known as the  
GNU/Linux Users Group (GLUG), is a student club of technology and FOSS  
enthusiasts in BMSIT..

More Read  
[http://swathiraotfscbmsit.blogspot.in/2014/08/after-campthe-volunteers-o...][6]

 [6]: http://swathiraotfscbmsit.blogspot.in/2014/08/after-campthe-volunteers-of-fsmk.html

#GLUE

Conducted Session on Android App Development - 9th August 2014

Conducted Introduction to Free software Session , 15 August 2014  
The session was mainly for 2nd year people who have just now joined GLUE
