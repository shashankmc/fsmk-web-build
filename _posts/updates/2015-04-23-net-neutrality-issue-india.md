---
layout: post
title: "Net neutrality issue in India"
date: 2015-04-23 06:04:02
category: updates
---

Net neutrality is the fundamental principle of the Internet which guarantees that no discrimination is practiced on the resources or applications that the users try to access!

Airtel is trying to undermine this basic principle by restricting the access by introducing differentiated price structure on accessing applications.

Facebook is trying to do the same via Internet.org. Airtel's zero platform or Facebooks's internet.org may end up blocking specific online content, services or applications (protocols, websites, etc.), or the users' freedom to publish information!

The Telecom Regulatory Authority(TRAI)is examining this issue to determine whether the 'openness' of the Internet is been compromised. Visit <a href="http://www.netneutrality.in/" target="_blank">netneutrality.in</a> for more detail.

As the time period for sending mail to TRAI ends on 24th April , we shall continue putting pressure on government in someother ways. MPs being our representatives in parlement we have to make our voice heard through them. Here's a <a href="http://j.mp/MailMyMP" target="_blank">sheet</a> with contact details of MPs, to make it easy for you to appeal to them via email and/or snail mail.
