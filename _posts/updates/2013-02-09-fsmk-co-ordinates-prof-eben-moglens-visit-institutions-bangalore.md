---
layout: post
title: "FSMK co-ordinates Prof. Eben Moglen's visit to Institutions in Bangalore"
date: 2013-02-09 19:00:28
category: updates
---

Who is Eben Moglen? His professional credentials say that he is a professor of Law and Legal History at Columbia University. He is the founder,  director – counsel  and chairman of the Software Freedom Law Centre(http://www.softwarefreedom.org/). His clients include numerous ‘pro bono’ institutions such as the Free Software Foundation. Eben began his career as a systems programmer for IBM in 1979. After receiving his Law degree, Eben worked as an IP (intellectual property) attorney for IBM in the Yorktown Lab of IBM Research. All this information is available if you do a Google search with his name.  There is more, however. Eben Moglen has authored numerous articles on free software. At the talks given by him (many can be found on Youtube), Moglen doesn’t restrict himself only to software. He touches on aspects of social evolution and relationships between the social classes as they have evolved in the modern times.  It was one of these articles which first got me interested in his work. Moglen authored ‘The dotCommunist Manifesto’ in 2003. Readers can find it here. http://emoglen.law.columbia.edu/publications/dcm.html

Indefatigable Eben readily agreed to FSMK’s invitation for participating in various programs held for almost 3 days when FSMK approached him. Eben Moglen was in Bangalore from 12 December 2008 to 14 December 2008. He came to Bangalore after he attended FSF conference in Trivandrum (Kerala).

Eben Moglen’s visit has a lot of significance to all the free software enthusiasts in Bangalore. Eben stretched himself hard throughout the very short time that he visited the city, to encourage and educate people about free and open source software. His simplicity and true leadership is amazing. This hacker, lawyer, historian in his activist avatar simply bowls people over with his commitment to the cause. His wit, humour, intelligence, humanitarianism, sense of justice - just wins over hearts wherever he is. FSMK members had lots of interactions with him.

 He was prepared to listen keenly to children in the slum computing center when planning their activities for the future. He was happy with the children and local community members, and did a serious planning session.  He spoke at the Indian Institute of Management-Bangalore with IEEE and spoke about how property is theft in the domain of 0 marginal cost. He spoke at the National Law School, Christ College and IISc. This being the centenary year of IISc and we being a 'knowledge economy' - his talk on self reliance and danger of patents is of utmost importance.  He didn’t spare the kannada new media as well. He spoke at the Kannada Bloggers Association. And he did all this over a period of only three days.

 The free software enthusiasts in Bangalore - if they are really touched by him - should take up the challenge - of converting this into ground work and in furtherance of this activist spirit and movement - starting from IISc, IIMB, NLSUI , Kannada Bloggers and Slum community children. The essence of his activities, as he explained –  
*‘If we have an army of scientists, management students, lawyers, writers and working class people hacking, who can stop us?’*

 
