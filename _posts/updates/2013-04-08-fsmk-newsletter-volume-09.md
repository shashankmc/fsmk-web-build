---
layout: post
title: "FSMK Newsletter Volume 09"
date: 2013-04-08 18:33:43
category: updates
---

In this August edition of the FSMK Newsletter, we are featuring some important debates pertaining to the bigger ideals of the Free Software movement.

The editorial tackles the double standards, or more aptly the digital divide that the IT revolution has brought about, keeping conditions in India and other developing countries in mind.

First of the article is the justification message posted by Greg Maxwell on bit-torrent indexing engine showing his support to Aaron Swartz; We are featuring it to show our solidarity to the entire Free Knowledge movement.

The debates on the social networks and the competition between the veterans and the rookies in this domain has been touched upon, projecting it to be the lines of next OS and Browser wars.

Interacting with the FOSS community for novice is another featured article in this edition. Along with one of our FSMK activists experiences at the sister organisation Swecha's 15 days workshop.

There are also some handy tech snippets and cool community images presented in the newsletter.

As always, give it a read, and spread it further;  
Looking forward to your feedback.

 

<noscript>
  <div class="statcounter"><a title="drupal analytics" href="http://statcounter.com/drupal/" target="_blank"><img class="statcounter" src="http://c.statcounter.com/7114502/0/54be0e0e/1/" alt="drupal analytics" ></a></div>
</noscript>

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK_Issue9_August_2011.pdf" style="margin: 0px; padding: 0px; text-decoration: underline; color: rgb(25, 134, 103); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 09</a>
