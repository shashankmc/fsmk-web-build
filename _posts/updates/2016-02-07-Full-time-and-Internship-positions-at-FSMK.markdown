---
layout: post
title:  "Full time and Internship positions at FSMK"
date:   2016-02-07 19:41 +0530
category: updates
discourse_topic_id: 191
---

Free Software Movement Karnataka(FSMK) is offering internships for students and graduates who would like to contribute to the free software movement through a range of tasks. These are a combination of both part-time and full-time positions.

Profile for interns at FSMK

    Help devise, conceptualise, implement and support technical and community events in Karnataka
    Support the growth of GNU/Linux User Groups (GLUG) related activities in different colleges all over Karnataka
    Assist with public relations(PR) and communications of the organisation through social media and other online-offline channels
    Help develop FSMK promotional pamphlets, digital media and other outreach material primarily in English with Kannada and Hindi and secondary languages
    Help run fund raisers
    Help prepare reports of activities
    Help implement the membership programme
    Maintenance of books of accounts, membership and donor database.
    Improvement and maintenance of the website and other network infrastructure
    Technical support for organisational operations
    Contribute code to technical projects hosted by FSMK

Requirements

    Understanding of the FSMK community and the free software philosophy
    Interest to work in a not-for-profit environment
    Leadership skills especially in working with voluntary groups
    Basic coding skills and ability to learn new skills to support projects
    Track record in writing, web editing and social media
    Interest in supporting volunteers and volunteer-run projects
    Ability to work effectively with a team of volunteers
    Commitment for at least three to six months. Project-based internships available.

Perks

    Ability to negotiate projects and tasks
    Contribute from outside Bangalore (depends on statement of purpose)
    Nominal stipend (on a case-by-case basis).
    Experience certificate will be provided at the end of the internship.

Apply

Prepare a statement of purpose within 250 words stating how you plan to contribute to FSMK. Include your vision statement for the organisation and how you plan to implement it. Send it via email to admin AT fsmk DOT org on or before 11th Feb 2016.

NOTE: Use only free document formats. Non-free formats will be discarded without notice.
