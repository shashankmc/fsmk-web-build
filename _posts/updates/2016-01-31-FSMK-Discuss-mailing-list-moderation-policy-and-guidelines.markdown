---
layout: post
title:  "FSMK Discuss mailing list moderation policy and guidelines"
date:   2016-01-31 18:18 +0530
category: updates
---

There is no moderation set for mailing list subscription. i.e anyone
with a valid email address can subscribe to FSMK Discuss mailing list.
All new subscriptions are set for moderation by default to avoid
spam(https://en.wikipedia.org/wiki/Email_spam) emails. Message
moderation will be unset after validating the first message from the
members provided following moderation benchmarks are met. Moderators are
bound to let the sender know if a message is being moderated which are
not comes under the scope of SPAM.

Following are the guidelines which we will use as a reference for
moderating mails. Mails which fall into one of the categories mentioned
below might be moderated. Moderating does not mean just deleting the
mail. Clarifications/corrections may be sought if necessary

1. Product promotions, service or business promotions which are of no
interest to the members of this mailing group.
An exception is to use [Commercial] or [Jobs] if the post is of a
commercial/employment nature but relevant to the theme of the group

2. Personal attacks or profanity.

3. Extreme deviation from the topic of discussion.

4. Private messages not meant to be discussed in the mailing list

5. Double posting (posting the same message again)
For any queries write to admin@fsmk.org
Please bear with us in case there is a delay in moderating mails.

Moderators
FSMK EC has full authority to create a moderation panel wherein the
panel should have more than one moderator.  Any complaints regarding
moderation can be registered at admin@fsmk.org

Mailing list guidelines

1. English is the medium of communication on this mailing list

2. Use a proper subject line. Emails with empty subject lines will be
put on moderation after the first violation

3. Do not troll in the mailing list
Link: http://en.wikipedia.org/wiki/Internet_troll

4. Use [OT] for off-topic for non-technical discussions. Don’t misuse
this to start flame wars or to troll in the mailing list.

5. Do not top-post:
Because it messes up the order in which people normally read text.
Link: http://en.wikipedia.org/wiki/Top-posting

Example of a top-post:
> > Why is top-posting
> > such a bad thing?
Top-posting.
> > What is the most
> > annoying thing in e-mail?

Use interleaved, trimmed posting
Hi,
— Foo Bar wrote:
> could design good
> application forms using tex
Sure.
> CTAN doesn’t seem to give me anything.
Check this example (Observing Time Application Form):
http://irtfweb.ifa.hawaii.edu/observing/applicationForms.php
—
Bar Foo

6. Do not over-quote:
Example of an over-quote:
>>> On Friday you said
>>> blah blah blah
>> On Saturday you said
>> foo foo foo
> On Sunday you said
> foobar

7. Do not post HTML messages

8. Do not recycle messages

9. Do not send attachments

10. Do not attach obnoxious, nonsensical legal disclaimers. If your
company uses disclaimers, don’t use your company ID for the mailing list.

11. Search for answers for your questions/problems in any search engine
before posting your query to the mailing list.

12. Do not post messages in all capital letters. Mails in Caps is
considered rude and is similar to shouting during a conversation.

13. Following these guidelines could be helpful
http://people.debian.org/~enrico/dcg/

14. Don’t send season’s greetings or birthday or social networking site
invites to the group. Its not mailing list etiquette.
We must use the above guidelines to maintain better readability, proper
archives, efficient bandwidth usage, etc.
If any of the above are not clear to you, Please read the detailed list
guidelines:
Link:
http://www.catb.org/~esr/faqs/smart-questions.html
