---
layout: post
title:  "Press statement on Free Basics issue"
date:   2016-01-01 15:47:41 +0530
category: press
---



Facebook has launched a massive campaign in support of Free Basics program – which is a re­branded version of the infamous internet.org. The entire Free Basics fiasco is because the government is abdicating its responsibility of ensuring that the Internet remains a public utility. It has become essential for us (FSMK­FSMI) to focus our energy on coming up with a credible alternative as different corporations and the different avatars of their "products" will want to monopolise the Internet.

While the Internet is being closed up slowly, the current approach by Facebook, related to Free Basics, and Google, w.r.t. their intervention with RailTel, will definitely kill the openness of the Internet. One sure victim will be the idea of Digital India as it will be forced to morph into an uglier version to factor in a closed Internet.

Till now we have been fighting such corporations and the retrograde polices of the government on their grounds using their rules which has been a disadvantage to the people. We will have to bring the fight to our turf.

Richard Stallman did this by coming up with the GNU project. It gained enough momentum to becoming self sustaining and inspire several generations. We call it free, as in freedom, software. We also have free/open hardware. We will have to build freedom spectrum here in India.

\#ForkTheInternet ­ We are going to start by building sustainable community­supported Internet infrastructure as an alternate to the existing Internet. We start by forking the last mile delivery of Internet services to the poor people. Free Basics claims that it is trying to provide free Internet to the poor people by giving them a limited set of services. We will pre­empt such ideas by showing a credible alternative that upholds the ideals of the people who designed the earlier versions of the Internet.

Using the Freedom Box and community band wifi/WiMax, we set up mesh networks using community sponsored Internet connections and ensure that the poor people have an opportunity to communicate with their loved ones at low/no cost. As a long term solution, we will also have to to fork the root DNS along with any other infrastructure necessary for inter­continental communication. This is difficult but not impossible.

As a starting point, we are celebrating Aaron Swartz anniversary by having a hackathon to come up with projects that will aid the successful forking of the Internet. This initiative will continue throughout this year and in association with different like­minded organisations. We will build alliances with all people and organisations who share our vision of freedom spectrum to ensure that this collective battle is decisively won in the favour of the people.

We urge the Government of India to uphold the very basics of NetNeutrality which are elementary to achieve a Digital India, increase innovation, employment generation and avoid a ruthless encroachment of the cyberspace.

Free Software Movement Karnataka(FSMK)
