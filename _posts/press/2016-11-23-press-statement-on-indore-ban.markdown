---
layout: post
title:  "Statement on Indore Magistrates ban order"
date:   2016-11-23 13:39:41 +0530
category: press
discourse_topic_id: 467
---

The District Magistrate of Indore has issued an order – Order/2956/RADM/2016, Indore/Date 14/11/2016 under Section 144 – banning any criticism on social media such as Twitter, Facebook, WhatsApp, etc.,on exchange of old currency that is “objectionable” or can “cause incitement”. The order is attached here. This, in effect, is a blanket ban on any criticism of the Government on its failure to provide sufficient new notes for the old Rs. 500 and Rs. 1,000 notes that it has demonetized and clearly putting the common man to immense hardship. The government now wants to clamp down on all criticism on its failures.

The use of Section 144 for censorship of social media also goes far beyond what the Supreme Court has held in its various judgments.The Supreme Court, in Madhu Limaye and Anr v. Ved Murti and Ors. ((1970) 3 SCC 746), held that the use of Section 144 is justified only for prevention of public disturbance or violence, and urgency as the only ground for using this section.

The state and central governments have been using Section 144 to impose censorship on Internet, sometime to the extent of shutting down the internet and going far beyond what its powers are under Section 144. It has now extended such powers, earlier used only for banning public assembly, to now attacking peoples rights of Freedom of Speech, guaranteed under the Section 19. It shows the desperation of the Central and the Madhya Pradesh governments that having failed in the elementary task of providing money to the people for conducting their day to day lives, they are resorting to such draconian measures to stifle all legitimate criticism.

The Free Software Movement Karnataka demands that this Order of the District Magistrate be immediately withdrawn and the Madhya Pradesh government issue an apology to the people for this action.

Shijil TV

Joint Secretary
