---
layout: post
title:  "Pass a law to ensure net neutrality"
date:   2015-04-22 18:04:01 +0530
category: press
---







Airtel India, one of the leading mobile and Internet providers in the country recently announced that it will charge pre paid mobile customers more for using certain over the top services – starting off with VoIP services (such as Skype and Viber). The costs are in the range of Rs. 16 per minute of Skype, a huge cost to those users who access the Internet through the mobile route. 

The implications of this move are clear – Airtel has indicated that it is willing to act as a gate-keeper of the Internet: restricting access to certain content / applications based on how much you can pay. It is only a step away from your service provider deciding which website you can access, which to speed up or slow down, based on payments. The provision of preferential access to content will lead to an environment where the big companies – Google, Facebook and so on – are able to ensure growth in their customer base while freezing out competition. 

This not only represents an increased cost to pre-paid users of Airtel mobile but also represents another instance in an increasing list of violations of the principle of network neutrality seen in India: virtually every telecom provider in India for instance, has an agreement with a content provider for preferential access to services (either in the form of data caps not being applied or preferential pricing).

The principle of network neutrality generally speaking, implies that service providers must treat all data packets on the Internet equitably. The principle prevents discrimination by a service provider based on the type of content accessed by a user and is essential to ensures plurality of content in the online space and to maintains competition in the online market. 

Failure to give effect to this principle is against the interest of the user, ensures that only rich content / media providers will survive (because they can pay to have their content delivered to the consumer) and will limit the uses to which the Internet can be put to based on the ability of the user to pay – a situation which can be disastrous for content creation and increasing Internet access / penetration in India. Without protecting net neutrality, small and medium business interests in India who want to use the Internet for their business will simply be frozen out by the existing big players.

Given the growing importance of the Internet and the e-commerce sector the Government must act with urgency to promote net neutrality and the public Internet. Implementation of the principle of network neutrality will also crucially, ensure greater emphasis on creation of infrastructure rather than implementation of unwarranted traffic management practices that could hinder the efforts of the Government in ensuring high speed Internet access in India.

In this regard, it may be noted that various countries around the world including the Netherlands, Brazil, Chile all have legislation protecting network neutrality, with many others putting in place other regulatory measures in this respect.

We, the undersigned, call on

The Department of Telecommunications and / or TRAI (who are both empowered to issue regulations on the issue) to issue appropriate notifications, orders or guidelines prohibiting service providers from implementing anti-competitive practices such as service providers providing preferential access to content / applications / services, including but not limited to zero-rating deals.

Vikram Vincent
General Secretary

