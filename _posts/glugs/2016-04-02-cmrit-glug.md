---
layout: post
title: "CMRIT GLUG"
date: 2016-04-02 06:01:27
category: glugs
---

CMRIT GLUG was started in the year 2015 by Abraar Syed,Ayesha Ishrath and Aishwarya Naidu. GLUG's main motto is to spread the importance of FOSS and to teach people different ways in which they can contribute to FOSS. Over the period of time GLUG has created a small community within our college where we all share and collaborate on ideas.
